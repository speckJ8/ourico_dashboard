import * as React from 'react'
import { Icon, Label } from 'semantic-ui-react'
import { DateRangePicker } from 'react-dates'


/**
 * View to choose a date interval
 * 
 * @prop {string}      label               label to describe the view
 * @prop {moment.date} data_from_date      initial date of the interval
 * @prop {moment.date} data_to_date        final date of the interval
 * @prop {function}    handle_dates_change callback that is called when date choices change
 */
export default class DateIntervalPicker extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            react_dates_focused_input: null
        }
    }

    render () {

        const label_style = {
            fontSize: '10pt',
            marginLeft: '0.5em',
            marginRight: '1em'
        }

        return (
            <div>
            <Label size='tiny' className="date_range_picker">
                <Icon name='calendar'/>
                <label style={label_style}>{this.props.label}</label>
                <DateRangePicker
                    small
                    required
                    noBorder
                    startDatePlaceholderText='Desde'
                    endDatePlaceholderText='Até'
                    isOutsideRange={() => false}
                    customArrowIcon={<Icon name='arrow right'/>}
                    startDate={this.props.data_from_date}
                    startDateId='dashboard_data_from'
                    endDate={this.props.data_until_date}
                    endDateId='dashboard_data_until'
                    onDatesChange={this.props.handle_dates_change}
                    focusedInput={this.state.react_dates_focused_input}
                    onFocusChange={ fi => {
                        this.setState({ react_dates_focused_input: fi })
                    }}
                />
                {/*<Menu><Menu.Item>{this.props.label}</Menu.Item></Menu> */}
            </Label>
            </div>
        )
    }
}
