import React from 'react'

import { Input, Select, TextArea } from 'semantic-ui-react'

import FormModal from '../FormModal'

import * as AlertsTriggersData from '../../../common/alerts_triggers_data'


export default class AlertAndTriggerModal extends React.Component {

    ALERT_FORM_SPEC = [
        {
            fields: [
                {
                    key        : 'name',
                    label      : 'Nome',
                    required   : true,
                    type       : 'text',
                    placeholder: 'Nome do sensor',
                    control    : Input
                },
                {
                    key        : 'level',
                    label      : 'Nível',
                    required   : true,
                    type       : 'text',
                    placeholder: 'Nível de urgência do alerta',
                    control    : Select,
                    options    : [
                        { key: 'NORMAL',   value: 'NORMAL',   text: 'Normal'  },
                        { key: 'WARNING',  value: 'WARNING',  text: 'Aviso'   },
                        { key: 'CRITICAL', value: 'CRITICAL', text: 'Crítico' }
                    ]
                }
            ],
            group_key: 'a'
        },
        {
            fields: [
                { // description
                    key     : 'description',
                    label   : 'Descrição',
                    required: false,
                    control : TextArea
                }
            ],
            group_key: 'c'
        }
    ]

    TRIGGER_FORM_SPEC = [
        {
            fields: [
                {
                    key        : 'name',
                    label      : 'Nome',
                    required   : true,
                    type       : 'text',
                    placeholder: 'Nome do sensor',
                    control    : Input
                }
            ],
            group_key: 'a'
        },
        {
            fields: [
                { // description
                    key     : 'description',
                    label   : 'Descrição',
                    required: false,
                    control : TextArea
                }
            ],
            group_key: 'c'
        }
    ]


    /**
     * Commit changes to server
     * 
     * @param {object} data data to save
     */
    save = async (data) => {
        let update = this.props.data !== undefined
        let err_msg = update
            ? 'Não foi possivel atualizar os dados'
            : 'Não foi possível guardar os dados'
        
        try {
            let result
            if (this.props.mode === 'alert')
                result = await AlertsTriggersData.save_alert(data, update)
            else
                result = await AlertsTriggersData.save_trigger(data, update)

            if (!result) {
                return err_msg
            } else {
                return true
            }

        } catch (exception) {
            console.log(exception)
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    }


    render () {
        let title
        let form_spec
        if (this.props.mode === 'alert') {
            title = this.props.data === undefined
                ? `Novo alerta`
                : `Editar alerta ${this.props.data.name}`
            form_spec = this.ALERT_FORM_SPEC
        } else {
            title = this.props.data === undefined
                ? `Novo trigger`
                : `Editar trigger ${this.props.data.name}`
            form_spec = this.TRIGGER_FORM_SPEC
        }
        
        return (
            <FormModal
                open={this.props.open}
                handle_close={this.props.handle_close}
                title={title}
                form_groups={form_spec}
                save={this.save}
                data={this.props.data}
                when_done={this.props.when_done}
            />
        )
    }

}