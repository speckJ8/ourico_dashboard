import React from 'react'

import { Input, TextArea } from 'semantic-ui-react'

import FormModal from '../FormModal'

import * as ActuatorData from '../../../common/actuator_data'


export default class ActuatorModal extends React.Component {

    FORM_SPEC = [
        {
            fields: [
                {
                    key        : 'name',
                    label      : 'Nome',
                    required   : true,
                    type       : 'text',
                    placeholder: 'Nome do sensor',
                    control    : Input
                }
            ],
            group_key: 'a'
        },
        {
            fields: [
                { // description
                    key     : 'description',
                    label   : 'Descrição',
                    required: false,
                    control : TextArea
                }
            ],
            group_key: 'c'
        }
    ]


    /**
     * Commit changes to server
     * 
     * @param {object} data the data to save
     */
    save = async (data) => {

        let update = this.props.data !== undefined
        let err_msg = update
            ? 'Não foi possivel atualizar os dados'
            : 'Não foi possível guardar os dados'

        try {
            
            if (!update)
                data.actuator_id = this.props.data.actuator_id
                
            let result = await ActuatorData.save_actuator(data, update)
            if (!result) {
                return err_msg
            } else {
                return true
            }

        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    }


    render () {
        let title = this.props.data === undefined
            ? `Novo atuador`
            : `Editar atuador ${this.props.data.name}`
        
        return (
            <FormModal
                open={this.props.open}
                handle_close={this.props.handle_close}
                title={title}
                form_groups={this.FORM_SPEC}
                save={this.save}
                data={this.props.data}
                when_done={this.props.when_done}
            />
        )
    }

}