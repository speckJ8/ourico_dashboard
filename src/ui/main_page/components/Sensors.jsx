import React from 'react'

import {

    Button,
    Dimmer,
    Grid,
    Icon,
    Input,
    List,
    Loader, 
    Message,
    Pagination

} from 'semantic-ui-react'

import { DataFetchState } from '../../../common/misc'
import * as SensorData    from '../../../common/sensor_data'
import SensorDetails      from './SensorDetails.jsx'
import SensorModal        from './SensorModal.jsx'

export default class Sensors extends React.Component {

    SENSORS_PER_PAGE = 10

    constructor (props) {
        super(props)

        this.state = {
            sensors                      : [],
            sensor_presenting            : undefined,
            pagination_position          : 0,

            data_fetch_state             : DataFetchState.PENDING,
            sensor_detail_data_fech_state: undefined,
            data_fetch_error             : 'Verifique a sua conexão á Internet',

            sensor_modal_open            : false
        }
    }


    /**
     * Get list of sensors
     * 
     * @returns {object[]} the data retrieved
     */
    get_data = async () => {
        try {
            let sensors = await SensorData.load()
            this.setState({
                sensors         : sensors,
                data_fetch_state: DataFetchState.DATA_FETCHED
            })
            return sensors
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    /**
     * Called when an item in the list of sensors is clicked
     * Gets the sensor's parameters and the motes where it is being used
     * 
     * @param {object} sensor data of the item clicked
     */
    handle_sensor_choice = async (sensor) => {
        this.setState({ sensor_detail_data_fech_state: DataFetchState.PENDING })

        try {
            
            if (sensor.with_details === true) {
                this.setState({
                    sensor_presenting            : sensor,
                    sensor_detail_data_fech_state: DataFetchState.DATA_FETCHED
                })
                return // data already fecthed
            }

            let sensor_details  = await SensorData.load_details(sensor.id)
            sensor.with_details = true
            Object.assign(sensor, sensor_details)

            this.setState({ 
                sensor_presenting            : sensor,
                sensor_detail_data_fech_state: DataFetchState.DATA_FETCHED
            })

        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    sensor_detail_data_fech_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ sensor_detail_data_fech_state: DataFetchState.FAILED })
            }
        }
    }


    /**
     * Called when data is changed in a sub component
     */
    handle_data_change = async () => {
        let current_id = this.state.sensor_presenting !== undefined
            ? this.state.sensor_presenting.id : -1
        this.setState({ sensor_presenting: undefined })

        let sensors = await this.get_data()
        
        if (this.state.current_id !== -1) {
            let new_sensor_presenting =
                sensors.find(m => m.id === current_id)

            if (new_sensor_presenting !== undefined)
                this.handle_sensor_choice(new_sensor_presenting)
        }
    }


    /**
     * Called when a search is made
     * 
     * @param {string} value value entered by the user
     */
    handle_search = (value) => {
        let new_sensors = this.state.sensors.slice()
        console.log('all the sensors', new_sensors)

        value = value.toLowerCase()
        new_sensors.forEach(sensor => {
            if (value == null || value === '') {
                sensor.to_show = true
                return
            } else if (!sensor.name.toLowerCase().includes(value)
                && (sensor.description == null || !sensor.description.toLowerCase().includes(value))) {
                sensor.to_show = false
            } else {
                sensor.to_show = true
            }
        })

        this.setState({ sensors: new_sensors })
    }


    async componentDidMount () {
        await this.get_data()
    }


    render () {

        if (this.state.data_fetch_state === DataFetchState.PENDING) {
            return (
                <Dimmer active inverted style={{ marginTop: '10em' }}>
                    <Loader content='A carregar dados' />
                </Dimmer>
            )
        } else if (this.state.data_fetch_state === DataFetchState.FAILED) {
            return (
                <Message negative style={{ margin: '5em' }}>
                    <Message.Header
                        content='Não foi possivel carregar a lista de sensores'
                    />
                    <p>{this.state.data_fetch_error}</p>
                </Message>
            )
        }

        let sensors_to_show = this.state.sensors.filter(sensor => sensor.to_show !== false)
        let nr_sensors_to_show = sensors_to_show.length
        sensors_to_show = sensors_to_show.slice( // just show `SENSORS_PER_PAGE` items per page
            this.state.pagination_position*this.SENSORS_PER_PAGE,
            this.state.pagination_position*this.SENSORS_PER_PAGE + this.SENSORS_PER_PAGE 
        )
        let nr_pages = Math.ceil(nr_sensors_to_show / this.SENSORS_PER_PAGE)

        return (
        <div>

            <SensorModal 
                open={this.state.sensor_modal_open}
                handle_close={() => this.setState({ sensor_modal_open: false })}
                when_done={this.handle_data_change}
            />

            <Grid divided>
                <Grid.Row>

                    <Grid.Column width={5} style={{ paddingLeft: '2em', paddingRight: '2em' }}>
                        <span>
                            <Input 
                                iconPosition='left' 
                                placeholder='Procurar...' 
                                onChange={(_, { value }) => this.handle_search(value)} 
                            >
                                <Icon name='search' />
                                <input />
                            </Input>
                            <Button
                                primary
                                floated='right'
                                icon labelPosition='right'
                                onClick={() => this.setState({ sensor_modal_open: true })}
                            >
                                <Icon name='plus' />
                                { 'Novo sensor'}
                            </Button>
                        </span>

                        <List divided relaxed selection >
                            {sensors_to_show.map(sensor => (
                                <List.Item 
                                    active={this.state.sensor_presenting !== undefined && this.state.sensor_presenting.id === sensor.id}
                                    key={sensor.id}
                                    onClick={() => this.handle_sensor_choice(sensor)}
                                >
                                    <List.Icon name='thermometer' size='large' verticalAlign='middle'/>
                                    <List.Content>
                                        <List.Header>{sensor.name}</List.Header>
                                        <List.Description>{sensor.description}</List.Description>
                                    </List.Content>
                                </List.Item>
                            ))}
                        </List>
                        <Pagination
                            activePage={this.state.pagination_position + 1} 
                            totalPages={nr_pages} 
                            size='mini'
                        />
                    </Grid.Column>

                    <Grid.Column width={11} style={{ paddingLeft: '2em', paddingRight: '2em' }}>
                        {this.state.sensor_detail_data_fech_state === DataFetchState.PENDING ? (
                            <Dimmer active inverted style={{ marginTop: '10em' }}>
                                <Loader content='A carregar dados' />
                            </Dimmer>
                        ) : (this.state.sensor_detail_data_fech_state === DataFetchState.FAILED ? (
                            <Message negative style={{ margin: '5em' }}>
                                <Message.Header
                                    content='Não foi possivel carregar os dados do sensor'
                                />
                                <p>{this.state.data_fetch_error}</p>
                            </Message>
                        ) : (this.state.sensor_presenting !== undefined ? (
                            <SensorDetails
                                sensor={this.state.sensor_presenting}
                                handle_data_change={this.handle_data_change}
                            />
                        ) : (
                            <Message info icon>
                                <Icon name='thermometer half' />
                                <Message.Content>
                                    <Message.Header content='Sensores' />
                                    <p>{'Clique num item na lista para ver seus dados'}</p>
                                </Message.Content>
                            </Message>
                        )))}
                    </Grid.Column>

                </Grid.Row>
            </Grid>
        </div>
        )
    }

}