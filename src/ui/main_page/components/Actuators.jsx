import React from 'react'

import {

    Button,
    Dimmer,
    Grid,
    Icon,
    Input,
    List,
    Loader, 
    Message,
    Pagination

} from 'semantic-ui-react'

import { DataFetchState } from '../../../common/misc'
import * as ActuatorData from '../../../common/actuator_data'

import ActuatorDetails from './ActuatorDetails'
import ActuatorModal   from './ActuatorModal'


export default class Actuators extends React.Component {

    ACTUATORS_PER_PAGE = 10

    constructor (props) {
        super(props)

        this.state = {
            actuators                      : [],
            actuator_presenting            : undefined,
            pagination_position            : 0,

            data_fetch_state               : DataFetchState.PENDING,
            actuator_detail_data_fech_state: undefined,
            data_fetch_error               : 'Verifique a sua conexão á Internet',

            actuator_modal_open            : false
        }
    }


    /**
     * Get the list of actuators
     */
    get_data = async () => {
        try {
            let actuators = await ActuatorData.load()
            this.setState({
                actuators         : actuators,
                data_fetch_state: DataFetchState.DATA_FETCHED
            })
            return actuators
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    /**
     * Called when an actuator in the list is chosen
     * 
     * @param {object} actuator the actuator chosen
     */
    handle_actuator_choice = async (actuator) => {
        this.setState({ actuator_detail_data_fech_state: DataFetchState.PENDING })

        try {
            
            if (actuator.with_details === true) {
                this.setState({
                    actuator_presenting            : actuator,
                    actuator_detail_data_fech_state: DataFetchState.DATA_FETCHED
                })
                return // data already fecthed
            }

            let actuator_details  = await ActuatorData.load_details(actuator.id)
            actuator.with_details = true
            Object.assign(actuator, actuator_details)

            this.setState({ 
                actuator_presenting            : actuator,
                actuator_detail_data_fech_state: DataFetchState.DATA_FETCHED
            })

        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    actuator_detail_data_fech_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ actuator_detail_data_fech_state: DataFetchState.FAILED })
            }
        }
    }


    /**
     * Called when data changes in a sub component
     */
    handle_data_change = async () => {
        let current_id = this.state.actuator_presenting !== undefined
            ? this.state.actuator_presenting.id : -1
        this.setState({ actuator_presenting: undefined })

        let actuators = await this.get_data()
        
        if (this.state.current_id !== -1) {
            let new_actuator_presenting =
                actuators.find(m => m.id === current_id)

            if (new_actuator_presenting !== undefined)
                this.handle_actuator_choice(new_actuator_presenting)
        }
    }


    /**
     * Called when a search is made
     * 
     * @param {string} value value entered by the user
     */
    handle_search = (value) => {
        let new_actuators = this.state.actuators.slice()

        value = value.toLowerCase()
        new_actuators.forEach(actuator => {
            if (value === null || value === undefined || value === '') {
                actuator.to_show = true
                return
            } else if (!actuator.name.toLowerCase().includes(value) 
                && !actuator.description.toLowerCase().includes(value)) {
                actuator.to_show = false
            } else {
                actuator.to_show = true
            }
        })

        this.setState({ actuators: new_actuators })
    }


    async componentDidMount () {
        await this.get_data()
    }


    render () {
        if (this.state.data_fetch_state === DataFetchState.PENDING) {
            return (
                <Dimmer active inverted style={{ marginTop: '10em' }}>
                    <Loader content='A carregar dados' />
                </Dimmer>
            )
        } else if (this.state.data_fetch_state === DataFetchState.FAILED) {
            return (
                <Message negative style={{ margin: '5em' }}>
                    <Message.Header
                        content='Não foi possivel carregar a lista de actuadores'
                    />
                    <p>{this.state.data_fetch_error}</p>
                </Message>
            )
        }

        let actuators_to_show = this.state.actuators.filter(a => a.to_show !== false)
        let nr_actuators_to_show = actuators_to_show.length
        actuators_to_show = actuators_to_show.slice( // just show `ACTUATORS_PER_PAGE` items per page
            this.state.pagination_position*this.ACTUATORS_PER_PAGE,
            this.state.pagination_position*this.ACTUATORS_PER_PAGE + this.ACTUATORS_PER_PAGE 
        )
        let nr_pages = Math.ceil(nr_actuators_to_show / this.ACTUATORS_PER_PAGE)


        return (
        <div>

            <ActuatorModal
                open={this.state.actuator_modal_open}
                handle_close={() => this.setState({ actuator_modal_open: false })}
                when_done={this.handle_data_change}
            />

            <Grid divided>
                <Grid.Row>
                    <Grid.Column width={5} style={{ paddingLeft: '2em', paddingRight: '2em' }}>
                        <span>
                            <Input 
                                iconPosition='left' 
                                placeholder='Procurar...' 
                                onChange={(_, { value }) => this.handle_search(value)} 
                            >
                                <Icon name='search' />
                                <input />
                            </Input>
                            <Button
                                primary
                                floated='right'
                                icon labelPosition='right'
                                onClick={() => this.setState({ actuator_modal_open: true })}
                            >
                                <Icon name='plus' />
                                { 'Novo atuador'}
                            </Button>
                        </span>

                        <List divided relaxed selection >
                            {actuators_to_show.map(actuator => (
                                <List.Item 
                                    active={this.state.actuator_presenting !== undefined && this.state.actuator_presenting.id === actuator.id}
                                    key={actuator.id}
                                    onClick={() => this.handle_actuator_choice(actuator)}
                                >
                                    <List.Icon name='wrench' size='large' verticalAlign='middle'/>
                                    <List.Content>
                                        <List.Header>{actuator.name}</List.Header>
                                        <List.Description>{actuator.description}</List.Description>
                                    </List.Content>
                                </List.Item>
                            ))}
                        </List>
                        <Pagination
                            activePage={this.state.pagination_position + 1} 
                            totalPages={nr_pages}
                            size='mini'
                        />
                    </Grid.Column>

                    <Grid.Column width={11} style={{ paddingLeft: '2em', paddingRight: '2em' }}>
                        {this.state.actuator_detail_data_fech_state === DataFetchState.PENDING ? (
                            <Dimmer active inverted style={{ marginTop: '10em' }}>
                                <Loader content='A carregar dados' />
                            </Dimmer>
                        ) : (this.state.actuator_detail_data_fech_state === DataFetchState.FAILED ? (
                            <Message negative style={{ margin: '5em' }}>
                                <Message.Header
                                    content='Não foi possivel carregar os dados do atuador'
                                />
                                <p>{this.state.data_fetch_error}</p>
                            </Message>
                        ) : (this.state.actuator_presenting !== undefined ? (
                            <ActuatorDetails
                                actuator={this.state.actuator_presenting}
                                handle_data_change={this.handle_data_change}
                            />
                        ) : (
                            <Message info icon>
                                <Icon name='wrench' />
                                <Message.Content>
                                    <Message.Header content='Atuadores' />
                                    <p>{'Clique num item na lista para ver seus dados'}</p>
                                </Message.Content>
                            </Message>
                        )))}
                    </Grid.Column>
                </Grid.Row>
            </Grid>

        </div>
        )

    }

}