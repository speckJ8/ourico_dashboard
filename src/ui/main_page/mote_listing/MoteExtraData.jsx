import React from 'react'

import { Dimmer, Grid, Loader, Menu, Message } from 'semantic-ui-react'

import TableWithPagination from '../TableWithPagination'
import DateIntervalPicker  from '../DateIntervalPicker'

import * as MoteData                   from '../../../common/motes_data'
import { DataFetchState, date_format } from '../../../common/misc'

import * as Moment from 'moment'



/**
 * Present status reports, errors, action results, triggers and alerts of a mote
 * 
 * @prop {number} mote_id id of the mote
 */
export default class MoteExtraData extends React.Component {

    status_reports_table_columns = [
        { text: 'De',  key: 'start_date_time' },
        { text: 'Até', key: 'end_date_time' },
        { text: 'Número de pacotes enviados',  key: 'nr_tx_packets' },
        { text: 'Número de bytes enviados',  key: 'nr_tx_bytes' },
        { text: 'Número de pacotes perdidos',  key: 'nr_lost_packets' },
        { text: 'Número de bytes perdidos',  key: 'nr_lost_bytes' }
    ]

    errors_table_columns = [
        { text: 'Data e Hora', key: 'date_time' },
        { text: 'Código de erro', key: 'err_code' },
        { text: 'Mensagem de erro', key: 'err_message' }
    ]

    action_results_table_columns = [
        { text: 'Data e Hora', key: 'date_time' },
        { text: 'Resultado', key: 'result' },
        { text: 'Ação', key: 'action' }
    ]

    alerts_table_columns = [
        { text: 'Data e Hora', key: 'date_time' },
        // { text: 'Ver Medições', key: 'meas_bt' }
    ]

    triggers_table_columns = [
        { text: 'Data e Hora', key: 'date_time' },
        // { text: 'Ver Medições e Ações', key: 'meas_act_bt' }
    ]


    constructor(props) {
        super(props)

        const from = Moment().subtract(7, 'days') // a week ago
        const to   = Moment() // today

        this.state = {
            data_fetch_state: DataFetchState.PENDING,
            data_fetch_error: 'Verifique a sua conexão á Internet',
            data: {},
            // get data since this date
            data_from_date: from,
            // get data until this date
            data_until_date: to,
        }
    }


    /**
     * Called when user changes data fetch interval
     * 
     * @param new_dates { startDate, endDate } interval to fetch data
     */
    handle_dates_change = async (new_dates) => {
        let start_date = new_dates.startDate !== null ?
            new_dates.startDate.hours(0) : null
        let end_date = new_dates.endDate !== null ?
            new_dates.endDate.hours(23).minutes(59) : null
        this.setState({
            data_from_date: start_date,
            data_until_date: end_date
        }, () => this.load_data())
    }


    /**
     * Fetch status reports, errors, triggers, alerts
     * and action results
     */
    load_data = async () => {
        try {
            let data = await MoteData.load_extra_data(
                this.props.mote_id,
                date_format(this.state.data_from_date),
                date_format(this.state.data_until_date),
            )
            console.log('extra data', data)
            this.setState({
                data: data,
                data_fetch_state: DataFetchState.DATA_FETCHED
            })
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    async componentDidMount () {
        await this.load_data()
    }


    render () {

        if (this.state.data_fetch_state === DataFetchState.PENDING) {
            return (
                <Dimmer active inverted style={{ marginTop: '10em' }}>
                    <Loader content='A carregar dados' />
                </Dimmer>
            )
        } else if (this.state.data_fetch_state === DataFetchState.FAILED) {
            return (
                <Message negative style={{ margin: '5em' }}>
                    <Message.Header
                        content='Não foi possivel dados do mote'
                    />
                    <p>{this.state.data_fetch_error}</p>
                </Message>
            )
        }

        return (
        <div>
            
            <Menu fluid size='small' secondary >
                <Menu.Item position='right' >
                    <DateIntervalPicker
                        data_from_date={this.state.data_from_date}
                        data_until_date={this.state.data_until_date}
                        handle_dates_change={this.handle_dates_change}
                        label='Ver dados no intervalo'
                    />
                </Menu.Item>
            </Menu>

            <Grid>

                <Grid.Row columns={1} >
                    <Grid.Column >
                        <TableWithPagination 
                            items_per_page={10}
                            name='Relatórios de estado'
                            header_fields={this.status_reports_table_columns}
                            data={this.state.data.status_reports.map(sr => ({
                                ...sr,
                                start_date_time: Moment(sr.start_date_time).format('HH:mm:ss DD-MM-YYYY'),
                                end_date_time  : Moment(sr.end_date_time).format('HH:mm:ss DD-MM-YYYY'),
                            }))}
                            row_key='date_time'
                        />
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row columns={1} >
                    <Grid.Column >
                        <TableWithPagination
                            items_per_page={10}
                            name='Erros'
                            header_fields={this.errors_table_columns}
                            data={this.state.data.errors.map(error => ({
                                ...error,
                                date_time: Moment(error.date_time).format('HH:mm:ss DD-MM-YYYY'),
                            }))}
                            row_key='date_time'
                        />
                    </Grid.Column>
                </Grid.Row>

                {this.state.data.action_results.map(au => (
                    <Grid.Row columns={1} key={au.num} >
                        <Grid.Column >
                            <TableWithPagination 
                                items_per_page={10}
                                name={`Ações do Atuator ${au.actuator.name}`}
                                description={au.actuator.description}
                                header_fields={this.action_results_table_columns}
                                data={au.results}
                                row_key='date_time'
                            />
                        </Grid.Column>
                    </Grid.Row>
                ))}

                {this.state.data.alerts.map(au => (
                    <Grid.Row columns={2} key={au.alert.id}>
                        <Grid.Column width={8} >
                            <TableWithPagination 
                                items_per_page={10}
                                row_key='date_time'
                                name={`Alerta ${au.alert.name}`}
                                description={au.alert.name}
                                header_fields={this.alerts_table_columns}
                                data={au.occurrences.map(oc => ({
                                    ...oc,
                                    date_time: Moment(oc.date_time).format('HH:mm:ss DD-MM-YYYY'),
                                }))}
                            />
                        </Grid.Column>
                    </Grid.Row>
                ))}

                {this.state.data.triggers.map(tu => (
                    <Grid.Row columns={2} key={tu.trigger.id}>
                        <Grid.Column width={8} >
                            <TableWithPagination 
                                items_per_page={10}
                                row_key='date_time'
                                name={`Trigger ${tu.trigger.name}`}
                                description={tu.trigger.description}
                                header_fields={this.triggers_table_columns}
                                data={tu.occurrences.map(oc => ({
                                    ...oc,
                                    date_time: Moment(oc.date_time).format('HH:mm:ss DD-MM-YYYY'),
                                }))}
                            />
                        </Grid.Column>
                    </Grid.Row>
                ))}

            </Grid>

        </div>
        )
    }

}