import React from 'react'

import { Select } from 'semantic-ui-react'

import * as MoteData from '../../../common/motes_data'

import FormModal from '../FormModal'


/**
 * Create a new connection between two motes
 * 
 * @prop {object}   mote         data of the mote
 * @prop {string}   open         indicates mote visibility
 * @prop {function} handle_close called when user requests to close the modal
 */
export default class MoteConnectionModal extends React.Component {


    FORM_SPEC = [
        {
            fields: [{ // name
                key        : 'other_mote',
                label      : 'Outro Mote',
                dont_update: true,
                required   : true, 
                placeholder: 'O mote ao qual este será conectado',
                control    : Select,
                options    : []
            }],
            group_key: 'a'
        },
        {
            fields: [
                { // measurements period
                    key        : 'direction',
                    label      : 'Direção da conexão',
                    required   : true,
                    control    : Select,
                    options    : [
                        { key: 'Bi',            value: 'Bi',            text: 'Bidirecional'                 },
                        { key: 'Here_to_There', value: 'Here_to_There', text: 'Deste mote para o outro mote' },
                        { key: 'There_to_Here', value: 'There_to_Here', text: 'Do outro mote para este mote' }
                    ]
                }
            ],
            group_key: 'b'
        }
    ]



    /**
     * Commit changes to server
     * 
     * @param {object} data data to save
     */
    save = async (data) => {

        console.log('received data', data)

        data.mote_id = this.props.mote.id

        let err_msg = 'Não foi possível guardar os dados'

        try {
            let result = await MoteData.save_connection(this.props.mote.id, data)
            if (!result) {
                return err_msg
            } else {
                return true
            }
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    }

    render () {

        let title = `Nova conexão para o mote ${this.props.mote.name}`

        this.FORM_SPEC[0].fields[0].options = this.props.mote_list
            .filter(mote => mote.id !== this.props.mote.id)
            .map(mote => ({
            key: mote.id, value: mote.id, text: mote.name
        }))

        return (
            <FormModal
                open={this.props.open}
                handle_close={this.props.handle_close}
                title={title}
                form_groups={this.FORM_SPEC}
                save={this.save}
                when_done={this.props.when_done}
            />
        )
    }

}