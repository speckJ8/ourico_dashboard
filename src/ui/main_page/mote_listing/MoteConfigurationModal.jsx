import React from 'react'

import { Input, Radio } from 'semantic-ui-react'

import * as MoteData from '../../../common/motes_data'

import FormModal from '../FormModal'


/**
 * Create a new configration for a mote
 * 
 * @prop {object}   mote         data of the mote
 * @prop {string}   open         indicates mote visibility
 * @prop {function} handle_close called when user requests to close the modal
 * @prop {function} data         configuration to edit (if undefined creation mode is set)
 */
export default class MoteConfigurationModal extends React.Component {


    FORM_SPEC = [
        {
            fields: [{ // name
                key        : 'name',
                label      : 'Nome',
                dont_update: true,
                required   : true, 
                placeholder: 'Nomes devem ser únicos',
                control    : Input,
                type       : 'text'
            }],
            group_key: 'a'
        },
        {
            fields: [
                { // measurements period
                    key        : 'measurement_period',
                    label      : 'Periodo entre medições',
                    required   : true,
                    placeholder: 'Em segundos',
                    control    : Input,
                    type       : 'number'
                },
                { // period to send data
                    key        : 'send_data_period',
                    label      : 'Periodo entre envio de dados',
                    required   : true, 
                    placeholder: 'Em segundos',
                    control    : Input,
                    type       : 'number'
                },
                { // status reports period
                    key        : 'status_report_period',
                    label      : 'Periodo entre relatórios',
                    required   : true, 
                    placeholder: 'Em segundos',
                    control    : Input,
                    type       : 'number'
                }
            ],
            group_key: 'b'
        },
        {
            fields: [
                { // use crypto choice
                    key     : 'use_crypto',
                    label   : 'Criptografar pacotes?',
                    required: false, 
                    control : Radio,
                    toggle  : true
                }
            ],
            group_key: 'c'
        }
    ]

    /**
     * Commit changes to server
     * 
     * @param {object} data data to save
     */
    save = async (data) => {

        data.use_crypto = data.use_crypto === undefined ? false : data.use_crypto
        data.active = data.active === undefined ? false : data.active
        data.mote_id    = this.props.mote.id
        
        let update = this.props.data !== undefined

        let err_msg = update
            ? 'Não foi possivel atualizar os dados'
            : 'Não foi possível guardar os dados'

        try {
            let result = await MoteData.save_configuration(this.props.mote.id, data, update)
            if (!result) {
                return err_msg
            } else {
                return true
            }
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    }

    render () {

        let title = this.props.data !== undefined && this.props.data.name !== undefined 
            ? `Atualizar configuração ${this.props.data.name}`
            : `Nova configuração para o mote ${this.props.mote.name}`

        return (
            <FormModal
                open={this.props.open}
                handle_close={this.props.handle_close}
                title={title}
                form_groups={this.FORM_SPEC}
                save={this.save}
                data={this.props.data}
                when_done={this.props.when_done}
            />
        )
    }

}