import React from 'react'

import { Dimmer, Input, Loader, Message, Modal, Select } from 'semantic-ui-react'

import * as MoteData from '../../../common/motes_data'

import { DataFetchState } from '../../../common/misc'

import FormModal from '../FormModal'


/**
 * Create or edit an sensor use in a mote
 * 
 * @prop {object}   mote         data of the mote
 * @prop {string}   open         indicates mote visibility
 * @prop {function} handle_close called when user requests to close the modal
 * @prop {object}   data         item to update (in case of update mode)
 * @prop {function} when_done    called when operation is executed successfully
 */
export default class SensorUseModal extends React.Component {


    FORM_SPEC = [
        {
            fields: [{ // name
                key        : 'sensor_id',
                label      : 'Sensor',
                dont_update: true,
                required   : true, 
                placeholder: 'O sensor que será adicionado',
                control    : Select,
                options    : []
            }],
            group_key: 'a'
        },
        {
            fields: [
                { // name of interface
                    key        : 'interface_name',
                    label      : 'Nome da interface',
                    placeholder: 'Interface onde o sensor será conectado',
                    required   : true,
                    type       : 'text',
                    control    : Input
                },
                { // number of interface
                    key        : 'interface_num',
                    label      : 'Número da interface',
                    placeholder: 'Entre 0 e 255. Deve ser único entre sensores no mote',
                    required   : true,
                    type       : 'number',
                    control    : Input
                }
            ],
            group_key: 'b'
        }
    ]

    constructor (props) {
        super(props)

        this.state = {
            sensors         : [],
            data_fetch_state: DataFetchState.PENDING,
            data_fetch_error: ''
        }
    }

    
    async componentDidMount () {
        try {

            let sensors = await MoteData.load_non_used_sensors(this.props.mote.id)
            this.setState({
                sensors          : sensors,
                data_fetch_state: DataFetchState.DATA_FETCHED
            })

        } catch (exception) {
            console.log(exception)
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    /**
     * Commit changes to server
     * 
     * @param {object} data data to save
     */
    save = async (data) => {

        if (data.interface_num > 255 || data.interface_num < 0)
            return '"Número de interface" deve estar entre 0 e 255 e ser único entre os sensores do mote'

        data.mote_id = this.props.mote.id

        let update = this.props.data !== undefined
        let err_msg = update
            ? 'Não foi possivel atualizar os dados'
            : 'Não foi possível guardar os dados'

        try {
            let result = await MoteData.save_sensor_use(this.props.mote.id, data, update)
            if (!result) {
                return err_msg
            } else {
                return true
            }
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    }

    render () {

        if (this.state.data_fetch_state === DataFetchState.PENDING) {
            if (this.props.open)
                return (
                    <Dimmer active inverted style={{ marginTop: '10em' }}>
                        <Loader content='A carregar dados' />
                    </Dimmer>
                )
            else return (<span hidden></span>)
        } else if (this.state.data_fetch_state === DataFetchState.FAILED) {
            return (
                <Modal
                    open={this.props.open}
                    style={{marginTop: '0 !important',marginLeft: 'auto',marginRight: 'auto'}}
                    onClose={this.props.handle_close}
                    header='Error'
                    content={
                        <Message negative style={{ margin: '5em' }}>
                            <Message.Header content='Não foi possivel carregar a lista de sensores' />
                            <p>{this.state.data_fetch_error}</p>
                        </Message>
                    }
                    actions={[
                        { key: 'ok', content: 'Ok', positive: true, onClick: this.props.handle_close }
                    ]}
                />
            )
        }

        let title = this.props.data !== undefined
            ? `Editor dados de utilização de sensor`
            : `Adicionar sensor ao mote ${this.props.mote.name}`

        this.FORM_SPEC[0].fields[0].options = this.state.sensors.map(sensor => ({
            key: sensor.id, value: sensor.id, text: sensor.name
        }))

        return (
            <FormModal
                open={this.props.open}
                handle_close={this.props.handle_close}
                title={title}
                form_groups={this.FORM_SPEC}
                save={this.save}
                data={this.props.data}
                when_done={this.props.when_done}
            />
        )
    }

}