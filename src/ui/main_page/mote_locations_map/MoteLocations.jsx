import React from 'react'

import Map from '../Map'

import * as MoteData from '../../../common/motes_data'
import { DataFetchState } from '../../../common/misc'

import {
    
    Button,
    Dimmer,
    Header,
    Icon,
    List,
    Loader,
    Menu,
    Message,
    Portal,
    Segment

} from 'semantic-ui-react'


export default class MoteLocations extends React.Component {

    /**
     * Card to show mote when information when clicked
     * 
     * @prop {object}   mote         data to present
     * @prop {object}   position     object with x and y pixel positions where to display card
     * @prop {boolean}  open         visibility controller
     * @prop {function} handle_close called when view is hidden
     */
    static MoteInfo = ({ mote, position, open, handle_close }) => {
        if (!open || mote === undefined) return (<span></span>)
        return (
        <Portal closeOnDocumentClick={false} open={open} onClose={handle_close}>
            <Segment.Group style={{ 
                left: `${position.x}px`,
                position: 'fixed',
                top: `${position.y}px`,
                zIndex: 1000,
                some_else: console.log(mote, position, open)
            }}>
                <Segment>
                    <Menu secondary >
                        <Menu.Item fitted><Header as='h5'>{mote.name}</Header></Menu.Item>
                        <Menu.Item fitted position='right'>
                            <Button icon basic compact floated='right' onClick={handle_close}>
                                <Icon name='x' />
                            </Button>
                        </Menu.Item>
                    </Menu>
                </Segment>
                <Segment secondary>
                    <List>
                        <List.Item>
                            <List.Content>
                                <List.Header>Estado</List.Header>
                                <List.Description>
                                    {mote.state ? 'Ativo' : 'Inativo'}
                                </List.Description>
                            </List.Content>
                        </List.Item>
                        <List.Item>
                            <List.Content>
                                <List.Header>Localização</List.Header>
                                <List.Description>
                                    {`Latitude: ${mote.location_lat}`}<br/>{`Longitude: ${mote.location_lng}`}
                                </List.Description>
                            </List.Content>
                        </List.Item>
                        <List.Item>
                            <List.Content>
                                <List.Header>IP Atual</List.Header>
                                <List.Description>
                                    {mote.current_ip !== null ? mote.current_ip : '-'}
                                </List.Description>
                            </List.Content>
                        </List.Item>
                    </List>
                </Segment>
            </Segment.Group>
        </Portal>
        )
    }

    constructor (props) {
        super(props)
        this.state = {
            data_fetch_state: DataFetchState.PENDING,
            data_fetch_error: 'Verifique a sua conexão á Internet',

            mote_info_open  : false,
            mote_info       : undefined,  // mote to present details
            info_position   : { x: 50, y: 50 } // info card position
        }
    }


    /**
     * Get all the motes
     */
    get_data = async () => {
        try {

            let motes = await MoteData.load_all()
            motes.forEach((mote) => { mote.show = true })
            this.setState({
                motes: motes,
                data_fetch_state: DataFetchState.DATA_FETCHED
            })

        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    async componentDidMount () {
        await this.get_data()
    }

    render () {
        if (this.state.data_fetch_state === DataFetchState.PENDING) {
            return (
                <Dimmer active inverted style={{ marginTop: '10em' }}>
                    <Loader content='A carregar dados' />
                </Dimmer>
            )
        } else if (this.state.data_fetch_state === DataFetchState.FAILED) {
            return (
                <Message negative style={{ margin: '5em' }}>
                    <Message.Header
                        content='Não foi possivel carregar a lista de motes'
                    />
                    <p>{this.state.data_fetch_error}</p>
                </Message>
            )
        }

        let markers = this.state.motes.map(mote => ({
            lat  : mote.location_lat,
            lng  : mote.location_lng,
            icon : mote.active ? '/active_mote.png' : '/inactive_mote.png',
            handle_click: (event, overlay) => {
                let point = overlay.getProjection().fromLatLngToContainerPixel(event.latLng) 
                this.setState({
                    mote_info_open: true,
                    mote_info: mote,
                    info_position: { x: point.x, y: point.y } 
                })
            }
        }))

        return (
            <div >
                <MoteLocations.MoteInfo 
                    open={this.state.mote_info_open}
                    mote={this.state.mote_info}
                    position={this.state.info_position}
                    handle_close={() => this.setState({ mote_info_open: false })}
                />
                <Map 
                    map_id='mote_locations_map'
                    markers={markers}
                    center={{ lat: markers[0].lat, lng: markers[0].lng }}
                    width='100vw'
                    height='81vh'
                    zoom={8}
                />
            </div>
        )
    }

}