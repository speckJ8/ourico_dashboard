import * as React from 'react'
import {

    Button,
    Dropdown,
    Grid,
    Header,
    Icon,
    Label,
    Menu,
    Popup,
    Segment,
    Statistic

} from 'semantic-ui-react'

import { Bar }  from 'react-chartjs-2'

import * as Moment from 'moment'
import * as StaticData from '../static_data.json'
import * as Settings   from '../../../common/app_settings'
import * as DashboardData from '../../../common/dashboard_data'


export default class DashboardMoteInfo extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            packets_by: Settings.get(Settings.Dashboard.MOTE_PACKETS_CHART_GROUP_BY),
            bytes_by  : Settings.get(Settings.Dashboard.MOTE_BYTES_CHART_GROUP_BY),
            packets   : []
        }
    }

    render () {

        let packets = this.props.data.packets.map(p => ({
            ...p, date_time: Moment(p.date_time)
        })).sort((a, b) => {
            if (b.date_time.isAfter(a.date_time)) return -1
            else if (a.date_time.isAfter(b.date_time)) return 1
            else return 0
        })
        console.log('packets', packets)

        let mote_name    = this.props.data.name
        let mote_active  = this.props.data.active
        let error_count  = this.props.data.errors.length
        let packet_count = packets.length
        let byte_count   = packets.reduce((acc, p) => acc + p.length, 0)


        let packets_per_time = DashboardData.count_data_by_date(
            // count packets by date
            this.state.packets_by, packets, (p) => 1
        )
        let bytes_per_time = DashboardData.count_data_by_date(
            // count bytes by date
            this.state.bytes_by, packets, (p) => p.length
        )
        let packet_types = {}
        packets.forEach((packet) => {
            let type_name = StaticData.global.packet_type_name[packet.packet_type]
            if (packet_types[type_name] === undefined)
                packet_types[type_name] = 1
            else
                packet_types[type_name] += 1
        })
        console.log('packets_per_time', packets_per_time)
        const packets_per_time_data = {
            labels: packets_per_time.labels,
            datasets: [{
                ...StaticData.dashboard.bar_charts_display.green,
                label: 'Número de pacotes',
                data: packets_per_time.values
            }]
        }

        const bytes_per_time_data = {
            labels: bytes_per_time.labels,
            datasets: [{
                ...StaticData.dashboard.line_charts_display.blue,
                label: 'Número de bytes',
                data: bytes_per_time.values
            }]
        }

        const packet_types_freq = {
            labels: Object.keys(packet_types),
            datasets: [{
                ...StaticData.dashboard.bar_charts_display.red,
                label: 'Frequência de tipos de pacotes',
                data: Object.values(packet_types).map(ft => (ft/packet_count).toPrecision(3)*100)
            }]
        }


        return (
        <Segment.Group style={{ marginBottom: '2em', display: this.state.display }}>
            <Segment className="dashboard_segment dashboard_segment__header" compact>
                <Menu secondary fluid>
                    <Menu.Item fitted className="dashboard_segment_mote__active">
                        {mote_active ? (
                            <Popup
                                trigger={<Label circular color='green'/>}
                                content='Mote Ativo'
                                on='hover'
                            />
                        ) : (
                            <Popup
                                trigger={<Label circular color='red' />}
                                content='Mote Inativo'
                                on='hover'
                            />
                        )}
                    </Menu.Item>
                    <Menu.Item position='left' fitted>
                        <Header as='h5' floated='left'>{mote_name}</Header>
                    </Menu.Item>
                    {/*<Menu.Item fitted>
                        <Popup
                            trigger={
                                <Link to={{ 
                                    pathname: '/motes', 
                                    search: `?pane=listing&mote_id=${mote_id}`,
                                    state: { pane: 'listing', mote: mote_id }
                                }}>
                                    <Button basic floated='right' compact icon='arrow right'/>
                                </Link>
                                }
                            content='Ver Dados do Mote'
                            on='hover'
                        />
                    </Menu.Item>*/}
                    <Menu.Item fitted>
                        <Button icon='x' basic floated='right' compact 
                            onClick={this.props.handle_hide}
                            style={{ padding: '0.6em' }}
                        />
                    </Menu.Item>
                </Menu>
            </Segment>

            <Segment  compact>
                <Grid centered>

                    <Grid.Row columns={2}>
                        {/* packets per time(hour, day, week) chart */}
                        <Grid.Column>
                                <span>
                                    Pacotes recebidos por{' '}
                                    <Dropdown 
                                        inline 
                                        options={StaticData.dashboard.time_chart_grouping_options}
                                        defaultValue={this.state.packets_by}
                                        onChange={(e, { value }) => this.setState({ packets_by: value })}
                                    />
                                </span>
                            <Bar 
                                data={packets_per_time_data} 
                                height={220}
                                options={{
                                    ...StaticData.dashboard.common_chart_conf,
                                    ...StaticData.dashboard.common_bar_chart_conf,
                                    tooltips: { callbacks: {
                                        label: (ti, dt) => `${ti.yLabel} pacotes`
                                    }}
                                }}
                            />
                        </Grid.Column>
                        {/* bytes per time(hour, day, week) chart */}
                        <Grid.Column>
                            <span>
                                Bytes recebidos por{' '}
                                <Dropdown
                                    inline
                                    options={StaticData.dashboard.time_chart_grouping_options}
                                    defaultValue={this.state.packets_by}
                                    onChange={(e, { value }) => this.setState({ bytes_by: value })}
                                />
                            </span>
                            <Bar 
                                data={bytes_per_time_data} 
                                height={220} 
                                options={{
                                    ...StaticData.dashboard.common_chart_conf,
                                    tooltips: { callbacks: {
                                        label: (ti, dt) => `${ti.yLabel} bytes`
                                    }}
                                }}
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={2} verticalAlign='middle' textAlign='center'>

                        <Grid.Column >
                            <Statistic.Group widths='3' size='mini' horizontal>
                                <Statistic >
                                    <Statistic.Value content={packet_count} />
                                    <Statistic.Label content={'Pacotes recebidos'}/>
                                </Statistic>
                                <Statistic>
                                    <Statistic.Value content={byte_count} />
                                    <Statistic.Label content='Bytes recebidos'/>
                                </Statistic>
                                {error_count > 0 ? (
                                    <Statistic color='red' >
                                        <Statistic.Value>
                                            <Icon name='exclamation' size='small'/>
                                            {error_count}
                                        </Statistic.Value>
                                        <Statistic.Label>Erros</Statistic.Label>
                                        <Button content='Ver detalhes' basic floated='right' compact />
                                    </Statistic>
                                ) : (
                                    <Statistic color='green'>
                                        <Statistic.Value>
                                            <Icon name='checkmark' size='small'/>
                                            {error_count}
                                        </Statistic.Value>
                                        <Statistic.Label>Erros</Statistic.Label>
                                    </Statistic>
                                )}
                            </Statistic.Group>
                        </Grid.Column>

                        {/* packet types frequency chart */}
                        <Grid.Column >
                            <Bar 
                                heigth={220} 
                                data={packet_types_freq}
                                options={{
                                    ...StaticData.dashboard.common_chart_conf,
                                    tooltips: { callbacks: {
                                        label: (ti, dt) => `${ti.yLabel}%`
                                    }}
                                }}
                            />
                        </Grid.Column>
                    </Grid.Row>

                </Grid>
            </Segment>

        </Segment.Group>
        )
    }

}
