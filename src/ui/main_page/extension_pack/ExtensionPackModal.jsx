import React from 'react'

import { Input, Select } from 'semantic-ui-react'

import FormModal from '../FormModal'

import * as ExtPackData from '../../../api/extension_pack'


export default class ExtensionPackModal extends React.Component {

    LANGS = [
        { key: 'python', value: 'python', text: 'Python' }
    ]

    FORM_SPEC = [
        {
            fields: [
                {
                    key: 'prog_language',
                    label: 'Linguagem de Programação',
                    required: true,
                    placeholder: 'Linguagem de Programação',
                    control: Select,
                    options: this.LANGS
                }
            ],
            group_key: 'a'
        },
        {
            fields: [
                {
                    key: 'code_repo_url',
                    label: 'URL do Repositório do código fonte',
                    required: true,
                    placeholder: 'Deve ser um repositório Git',
                    control: Input,
                    type: 'url'
                }
            ],
            group_key: 'a'
        },
    ]


    save = async (data) => {
        let update = this.props.data !== undefined
        let err_msg = update
            ? 'Não foi possivel atualizar os dados'
            : 'Não foi possível guardar os dados'

        try {
            let result
            if (update) {
                result = await ExtPackData.update(data.prog_language, data.code_repo_url)
            } else {
                result = await ExtPackData.create(data.prog_language, data.code_repo_url)
            }

            if (!result) {
                return err_msg
            } else {
                return true
            }

        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    } 


    render () {
        let title = this.props.data !== undefined && this.props.data.name !== undefined
            ? `Atualizar Extension Pack ${this.props.data.name}`
            : `Criar Extension Pack`

        return (
            <FormModal
                open={this.props.open}
                handle_close={this.props.handle_close}
                title={title}
                form_groups={this.FORM_SPEC}
                save={this.save}
                data={this.props.data}
                when_done={this.props.when_done}
            />
        )
    }
}