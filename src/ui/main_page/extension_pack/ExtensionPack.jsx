import React from 'react'

import {
    Button,
    Dimmer,
    Dropdown,
    Grid,
    Icon,
    Label,
    Loader,
    Message
} from 'semantic-ui-react'

import { DataFetchState } from '../../../common/misc'

import * as ExtPackData from '../../../api/extension_pack'
import * as Moment      from 'moment'

import ExtensionPackModal  from './ExtensionPackModal'
import SubscriptionModal   from './SubscriptionModal'
import TableWithPagination from '../TableWithPagination'


export default class ExtensionPack extends React.Component {


     LOG_TABLE_COLUMNS = [
        { key: 'level',     text: 'Estado'      },
        { key: 'date_time', text: 'Data e Hora' },
        { key: 'message',   text: 'Mensagem'    }
    ]

    constructor (props) {
        super(props)

        this.state = {
            data_fetch_state: DataFetchState.PENDING,
            mote_pres_data_fetch_state: null,
            data_fetch_error: 'Verifique a sua conexão á Internet',

            extension_pack: undefined,

            create_modal_open: false,
            sub_modal_open   : false,
            unsub_modal_open : false,

            changing_state: false
        }
    }


    get_data = async () => {
        try {
            let extension_pack = await ExtPackData.load_data()
            this.setState({
                data_fetch_state: DataFetchState.DATA_FETCHED,
                extension_pack: extension_pack
            })
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    change_state = async (activate) => {
        this.setState({ changing_state: true })
        try {
            if (activate)
                await ExtPackData.start()
            else await ExtPackData.stop()
            this.handle_data_change()
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
        this.setState({ changing_state: false })
    }


    handle_data_change = async () => {
        await this.get_data()
    }





    async componentDidMount () {
        await this.get_data()
    }


    render () {
        if (this.state.data_fetch_state === DataFetchState.PENDING) {
            return (
                <Dimmer active inverted style={{ marginTop: '10em' }}>
                    <Loader content='A carregar dados' />
                </Dimmer>
            )
        } else if (this.state.data_fetch_state === DataFetchState.FAILED) {
            return (
                <Message negative style={{ margin: '5em' }}>
                    <Message.Header
                        content='Erro na comunicação com o servidor'
                    />
                    <p>{this.state.data_fetch_error}</p>
                </Message>
            )
        }

        if (this.state.extension_pack === undefined) {
            return (
            <div>
                <ExtensionPackModal
                    open={this.state.create_modal_open}
                    handle_close={() => this.setState({ create_modal_open: false })}
                    when_done={this.handle_data_change}
                />
                <Grid padded={false} centered>
                    <Grid.Row>
                        <Grid.Column>
                            <Message info style={{ margin: '5em 5em 1em 5em' }}>
                                <Message.Header content='Sem Extension Pack' />
                                <p>Deve primeiro criar o extension pack</p>
                            </Message>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={1}>
                        <Grid.Column width={3}>
                            <Button 
                                primary icon labelPosition='right' 
                                onClick={() => this.setState({ create_modal_open: true })}>
                                <Icon name='certificate'/>
                                Criar Extension Pack
                            </Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
            )
        }

        return (
        <div>
            <ExtensionPackModal
                open={this.state.update_modal_open}
                handle_close={() => this.setState({ update_modal_open: false })}
                when_done={this.handle_data_change}
                data={this.state.extension_pack}
            />
            <SubscriptionModal
                open={this.state.sub_modal_open || this.state.unsub_modal_open}
                handle_close={() => this.setState({ unsub_modal_open: false, sub_modal_open: false })}
                sub={this.state.sub_modal_open}
            />
            <Grid style={{ margin: '1em 0 1em 1em' }}>
                <Grid.Row>
                    <Grid.Column width={5}>
                    </Grid.Column>
                    <Grid.Column width={10}>
                        {this.state.extension_pack.active ? (
                            <Button as='div' labelPosition='right' floated='right'
                                onClick={() => this.change_state(false)}
                                disabled={this.state.changing_state}>
                                <Button basic color='green'
                                    disabled={this.state.changing_state}
                                    loading={this.state.changing_state}>
                                    <Icon name='toggle on'/>On
                                </Button>
                                <Label as='a' basic color='green'>
                                    {'Desativar'}
                                </Label>
                            </Button>
                        ) : (
                            <Button as='div' labelPosition='right' floated='right'
                                onClick={() => this.change_state(true)}
                                disabled={this.state.changing_state}>
                                <Button color='red'
                                    disabled={this.state.changing_state}
                                    loading={this.state.changing_state}>
                                    <Icon name='toggle off'/>Off
                                </Button>
                                <Label as='a' basic >
                                    {'Ativar'}
                                </Label>
                            </Button>
                        )}
                        <Button labelPosition='left' icon floated='right'
                            onClick={() => this.setState({ update_modal_open: true })}
                        >
                            <Icon name='edit' />Atualizar
                        </Button>
                    </Grid.Column>
                    <Grid.Column width={1} style={{ marginTop: '6px' }}>
                        <Dropdown item icon='ellipsis vertical' direction='left'>
                            <Dropdown.Menu>
                            <Dropdown.Item onClick={() => this.setState({ sub_modal_open: true })}>
                                Adicionar subscrição a eventos de motes
                            </Dropdown.Item>
                            <Dropdown.Item onClick={() => this.setState({ unsub_modal_open: true })}>
                                Remover subscrição a eventos de motes
                            </Dropdown.Item>
                            <Dropdown.Item onClick={() => this.setState({  })}>
                                Enviar pedido
                            </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}>
                        <TableWithPagination
                            name='Logs'
                            items_per_page={10}
                            header_fields={this.LOG_TABLE_COLUMNS}
                            row_key='date_time'
                            data={this.state.extension_pack.logs.map(log => ({
                                ...log,
                                row_props: { error: log.level === 'error', warning: log.level === 'warning' },
                                date_time: Moment(log.date_time).format('HH:mm:ss DD-MM-YYYY'),
                                level: log.level === 'info'
                                    ? 'Ok' : (log.level === 'warning' ? 'Aviso' : 'Erro'),
                                handle_delete: () => this.delete_log(log)
                            }))}
                            actions='delete'
                        />
                    </Grid.Column>
                </Grid.Row>

            </Grid>
        </div>
        )
    }

}