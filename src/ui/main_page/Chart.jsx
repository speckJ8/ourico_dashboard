import React from 'react'

import { Bar, Line, Pie } from 'react-chartjs-2'

import { Dropdown } from 'semantic-ui-react'

/**
 * Chart view that can change betwee pie, bar an line types
 * 
 * @prop {object}   data            data to be displayed
 * @prop {string}   default_chart   default type of chart to be displayed
 * @prop {string[]} accepted_charts to limit chart types that user can choose
 * @prop {string}   units           units of the dataset
 * @prop {string}   x               name of the attribute in the x axis
 * @prop {string}   y               name of the attribute in the y axis
 */
export default class Chart extends React.Component {

    CHART = {
        conf: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: { suggestedMin: 0 }
                }],
                xAxes: [{
                    display: false
                }]
            }
        }
    }


    BAR_CHART = {
        dislay: {
            "backgroundColor": "rgba(36,41,255,0.2)",
            "borderColor": "rgba(36,41,255,1)",
            "borderWidth": 1,
            "hoverBackgroundColor": "rgba(36,41,255,0.4)",
            "hoverBorderColor": "rgba(36,41,255,1)"
        }
    }

    LINE_CHART = {
        display: {
            "backgroundColor": "rgba(36,150,46,0.2)",
            "borderColor": "rgba(36,150,46,1)",
            "borderWidth": 1,
            "borderJoinStyle": "miter",
            "hoverBackgroundColor": "rgba(36,150,46,0.4)",
            "hoverBorderColor": "rgba(36,150,46,1)"
        }
    }

    PIE_CHART = {
        dislay: {
            "legend": {
                "display": false
            },
            "backgroundColor": [
                "rgba(0, 0, 255, 0.4)",
                "rgba(0, 255, 0, 0.4)",
                "rgba(255, 0, 0, 0.4)",
                "rgba(100, 100, 100, 0.4)",
                "rgba(50, 100, 150, 0.4)",
                "rgba(150, 100, 50, 0.4)",
                "rgba(120, 60, 40, 0.4)",
                "rgba(40, 60, 120, 0.4)"
            ],
            "hoverBackgroundColor": [
                "rgba(0, 0, 255, 0.1)",
                "rgba(0, 255, 0, 1)",
                "rgba(255, 0, 0, 1)",
                "rgba(100, 100, 100, 1)",
                "rgba(50, 100, 150, 1)",
                "rgba(150, 100, 50, 1)",
                "rgba(120, 60, 40, 1)",
                "rgba(40, 60, 120, 1)"
            ]
        }
    }


    constructor (props) {
        super(props)

        this.state = {
            data           : [],
            accepted_charts: [],
            chart          : 'line'
        }
    }


    static getDerivedStateFromProps (props, _) {

        let CHART_TYPES = [
            { key: 'pie',  value: 'pie',  text: 'Pie',    icon: 'pie chart'  },
            { key: 'line', value: 'line', text: 'Linear', icon: 'line chart' },
            { key: 'bar',  value: 'bar',  text: 'Barras', icon: 'bar chart'  }
        ]

        let data = {
            labels: [],
            datasets: [{ label: '', data : [] }]
        }

        props.data.forEach(d => {
            data.labels.push(d[props.x])
            data.datasets[0].data.push(d[props.y])
        })

        let accepted_charts = CHART_TYPES.filter(
            ch => props.accepted_charts.find(ac => ac === ch.key || ac === 'all')
        )

        let chart = props.default_chart !== undefined 
            ? props.default_chart : 'line'

        return ({
            data           : data,
            accepted_charts: accepted_charts,
            chart          : chart
        })
    }

    render () {
        let chart

        if (this.state.chart === 'bar') {
            Object.assign(this.state.data.datasets[0], { ...this.BAR_CHART.dislay })
            chart = (
                <Bar 
                    data={this.state.data}
                    options={{
                        ...this.CHART.conf,
                        tooltips: {
                            callbacks: {
                                label: (ti, _) => `${ti.yLabel} ${this.props.units}`
                            }
                        }
                    }}
                />
            )
        } else if (this.state.chart === 'line') {
            Object.assign(this.state.data.datasets[0], { ...this.LINE_CHART.display })
            chart = (
                <Line
                    data={this.state.data}
                    options={{
                        ...this.CHART.conf,
                        tooltips: {
                            callbacks: {
                                label: (ti, _) => `${ti.yLabel} ${this.props.units}`
                            }
                        }
                    }}
                />
            )
        } else if (this.props.chart === 'pie') {
            chart = (
                <Pie
                    data={this.state.data}
                    options={{
                        ...this.CHART.conf,
                        tooltips: {
                            callbacks: {
                                label: (ti, _) => `${ti.yLabel} ${this.props.units}`
                            }
                        }
                    }}
                />
            )
        }

        return (
            <div>
                <span style={{ float: 'right' }} >
                    {'Tipo de gráfico'}{' '}{' '}
                    <Dropdown
                        labeled
                        button
                        floated='right'
                        compact
                        size='mini'
                        options={this.state.accepted_charts}
                        value={this.state.chart}
                        onChange={(_, { value }) => this.setState({ chart: value })}
                    />
                </span>
                {chart}
            </div>
        )
    }

}