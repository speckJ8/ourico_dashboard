
/**
 * Keys of global values
 */
export const Global = {
    ACTIVE_EVENTS: 'ourico.storage.global.active_events'
}


/**
 * Retrieve a value from storage
 * 
 * @param {string} key
 * 
 * @return {object | null}
 */
export function get (key) {
    let value = null
    try {
        value = window.localStorage.getItem(key)
        value = JSON.parse(value)
    } catch (error) {
        console.error('Unable to use localStorage', error)
    }
    return value
}


/**
 * Save a value in storage
 * 
 * @param {string} key 
 * @param {object} value 
 */
export function set (key, value) {
    try {
        window.localStorage.setItem(key, JSON.stringify(value))
    } catch (error) {
        console.error('Unable to use localStorage', error)
    }
}