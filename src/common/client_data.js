import * as Common  from '../api/common'
import * as Data    from '../api/data'
import * as Control from '../api/control'


/**
 * Get list of clients
 */
export async function load () {
    let query = { query: [
        {
            resource: Data.Resources.Client.name,
            fields  : Data.Resources.Client.fields.all,
        },
        {
            resource: Data.Resources.ApiClient.name,
            fields  : Data.Resources.ApiClient.fields.all,
        }
    ]}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return ({ users: results.data[0], apps: results.data[1] })
}


/**
 * Get client logs
 * 
 * @param {number} client_id id of the client
 * @param {string} type      'user' or 'app'
 * 
 * @returns {object[]}
 */
export async function load_details (client_id, type) {
    let query
    if (type === 'user') {
        query = { query: {
            resource: Data.Resources.UserLogs.name,
            fields  : Data.Resources.UserLogs.fields.all,
            filters : [{
                [Data.Resources.UserLogs.fields.client_id]: client_id
            }],
            options : [
                { orddesc: Data.Resources.UserLogs.fields.date_time }
            ]
        }}
    } else {
        query = { query: {
            resource: Data.Resources.AppLogs.name,
            fields: Data.Resources.AppLogs.fields.all,
            filters: [{
                [Data.Resources.AppLogs.fields.api_client_id]: client_id
            }],
            options: [
                { orddesc: Data.Resources.AppLogs.fields.date_time }
            ]
        }}
    }

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return ({ logs: results.data[0] })
}


/**
 * Update or create a client
 * 
 * @param {object}  data   client data
 * @param {string}  type   client type 'user' or 'app'
 * @param {boolean} update update or create
 * 
 * @return {boolean}
 */
export async function save_client (data, type, update=false) {

    let result

    if (update) {
        let op_data 
        if (type === 'user') {
            op_data = { data: {
                type    : Control.ClientType.User,
                id      : data.id,
                new_data: {
                    [Data.Resources.Client.fields.password]            : data.password,
                    [Data.Resources.Client.fields.rt_events_capability]: data.rt_events_capability,
                    [Data.Resources.Client.fields.write_capability]    : data.write_capability,
                    [Data.Resources.Client.fields.compute_capability]  : data.compute_capability
                }
            }}
        } else {
            op_data = { data: {
                type: Control.ClientType.App,
                id: data.id,
                new_data: {
                    [Data.Resources.ApiClient.fields.rt_events_capability]: data.rt_events_capability,
                    [Data.Resources.ApiClient.fields.write_capability]    : data.write_capability,
                    [Data.Resources.ApiClient.fields.compute_capability]  : data.compute_capability
                }
            }}
        }

        result = await Common.http_request('post', Control.update_client_url, JSON.stringify(op_data))

    } else {

        let op_data

        if (type === 'user') {
            op_data = {
                resource: Data.Resources.Client.name,
                data    : data
            }
        } else {
            op_data = {
                resource: Data.Resources.ApiClient.name,
                data: data
            }
        }

        result = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))
    }

    if (result.is_error()) {
        let err = { error_msg: result.error_msg }
        throw err
    }

    return true
}


/**
 * Delete a client
 * 
 * @param {number} client_id id of client
 * @param {string} type      'user' or 'app'
 * 
 * @return {boolean}
 */
export async function delete_client (client_id, type) {
    let op_data = { data: {
        id  : client_id,
        type: type === 'user' ? Control.ClientType.User : Control.ClientType.App
    }}

    let result = 
        await Common.http_request('post', Control.delete_client_url, JSON.stringify(op_data))
    
    if (result.is_error()) {
        let err = { error_msg: result.error_msg }
        throw err
    }

    return true
}


export async function delete_log (log, client_type) {
    let op_data

    if (client_type === 'user') {
        op_data = {
            resource: Data.Resources.UserLogs.name,
            where   : [
                { [Data.Resources.UserLogs.fields.date_time]: log.date_time },
                { [Data.Resources.UserLogs.fields.client_id]: log.client_id }
            ]
        }
    } else {
        op_data = {
            resource: Data.Resources.AppLogs.name,
            where: [
                { [Data.Resources.AppLogs.fields.date_time]    : log.date_time     },
                { [Data.Resources.AppLogs.fields.api_client_id]: log.api_client_id }
            ]
        }
    }

    let results =
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}