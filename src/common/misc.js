/**
 * Constants used to indicate the state of a data fetching operation
 */
const DataFetchState = {
    // request pending
    PENDING     : 0,
    // request successful
    DATA_FETCHED: 1,
    // request failed
    FAILED      : 2
}


/**
 * Format Moment.js date as YYYY-MM-DD HH:mm:ss
 */
export function date_format(date) {
    return date.format('YYYY-MM-DD HH:mm:ss')
}


export { DataFetchState }
