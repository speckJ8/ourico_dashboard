import * as Common from '../api/common'
import * as Data   from '../api/data'


/**
 * Get the list of triggers and alerts
 */
export async function load () {
    let query = { query: [
        {
            resource: Data.Resources.Alert.name,
            fields  : Data.Resources.Alert.fields.all,
        },
        {
            resource: Data.Resources.Trigger.name,
            fields  : Data.Resources.Trigger.fields.all,
        }
    ]}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return ({ alerts: results.data[0], triggers: results.data[1] })
}


/**
 * Get one alert
 * 
 * @param {number} alert_id id of the alert
 * 
 * @return {object}
 */
export async function get_alert (alert_id) {
    let query = {
        query: {
            "resource": Data.Resources.Alert.name,
            "fields"  : Data.Resources.Alert.fields.all,
            "filters" : [{
                [Data.Resources.Alert.fields.id]: alert_id
            }]
        }
    }

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0][0]
}


/**
 * Get one trigger
 * 
 * @param {number} trigger_id id of the trigger
 * 
 * @return {object}
 */
export async function get_trigger (trigger_id) {
    let query = {
        query: {
            "resource": Data.Resources.Trigger.name,
            "fields"  : Data.Resources.Trigger.fields.all,
            "filters" : [{
                [Data.Resources.Trigger.fields.id]: trigger_id
            }]
        }
    }

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0][0]
}



export async function save_alert (alert, update=false) {
    let op_data = {
        resource: Data.Resources.Alert.name,
        data    : alert
    }

    let results
    if (update) {
        op_data.where = [{
            [Data.Resources.Alert.fields.id]: alert.id
        }]

        op_data.data = {
            [Data.Resources.Alert.fields.name]       : alert.name,
            [Data.Resources.Alert.fields.description]: alert.description,
            [Data.Resources.Alert.fields.level]      : alert.level
        }

        results = await Common.http_request('put', Data.update_url, JSON.stringify(op_data))
    } else {
        results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))
    }

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


export async function save_trigger (trigger, update=false) {
        let op_data = {
        resource: Data.Resources.Trigger.name,
        data    : trigger
    }

    let results
    if (update) {
        op_data.where = [{
            [Data.Resources.Trigger.fields.id]: trigger.id
        }]

        op_data.data = {
            [Data.Resources.Trigger.fields.name]       : trigger.name,
            [Data.Resources.Trigger.fields.description]: trigger.description,
            [Data.Resources.Trigger.fields.level]      : trigger.level
        }

        results = await Common.http_request('put', Data.update_url, JSON.stringify(op_data))
    } else {
        results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))
    }

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


export async function delete_alert (alert_id) {
    let op_data = {
        resource: Data.Resources.Alert.name,
        where   : [
            { [Data.Resources.Alert.fields.id]: alert_id }
        ]
    }

    let results = 
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))
    
    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


export async function delete_trigger (trigger_id) {
    let op_data = {
        resource: Data.Resources.Trigger.name,
        where   : [
            { [Data.Resources.Trigger.fields.id]: trigger_id }
        ]
    }

    let results = 
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))
    
    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}