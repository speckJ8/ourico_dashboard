import * as Common from '../api/common'
import * as Data   from '../api/data'


/**
 * Get the list of actuators
 * 
 * @returns {object[]} the list of actuators 
 */
export async function load () {
    let query = { query: { 
        resource: Data.Resources.Actuator.name,
        fields  : Data.Resources.Actuator.fields.all
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0]
}


/**
 * Get one actuator
 * 
 * @param {number} actuator_id id of the actuator
 * 
 * @return {object}
 */
export async function get_one (actuator_id) {
    let query = {
        query: {
            "resource": Data.Resources.Actuator.name,
            "fields"  : Data.Resources.Actuator.fields.all,
            "filters" : [{
                [Data.Resources.Actuator.fields.id]: actuator_id
            }]
        }
    }

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0][0]
}



/**
 * Get actions of an actuator and the motes where it is being used
 * 
 * @param {number} actuator_id the id of the actuator
 * 
 * @returns {object}
 * @throws {object}
 */
export async function load_details (actuator_id) {
    let query = { query: {
        resource: Data.Resources.Actuator.name,
        filters : [{
            [Data.Resources.Actuator.fields.id]: actuator_id
        }],
        fields  : [
            Data.Resources.Actuator.fields.id,
            Data.Resources.Actuator.fields.name
        ],
        include : [
            { // actions
                resource: Data.Resources.Actuator.related.actions,
                fields  : Data.Resources.Action.fields.all
            },
            { // motes where it is being used
                resource: Data.Resources.Actuator.related.uses,
                fields  : Data.Resources.ActuatorUse.fields.all,
                include : [{
                    resource: Data.Resources.ActuatorUse.related.mote,
                    fields  : [Data.Resources.Actuator.fields.name]
                }]
            }
        ]
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0][0]
}


/**
 * Update a actuator or save a new record
 * 
 * @param {object}  actuator data to save
 * @param {boolean} update   entry will be created if false
 * 
 * @return {boolean}
 * @throws {string}
 */
export async function save_actuator (actuator, update=false) {
    let op_data = {
        resource: Data.Resources.Actuator.name,
        data    : actuator
    }

    let results

    if (update) {
        op_data.where = [{
            [Data.Resources.Actuator.fields.id]: actuator.id
        }]

        op_data.data = {
            [Data.Resources.Actuator.fields.name]       : actuator.name, 
            [Data.Resources.Actuator.fields.description]: actuator.description
        }

        results = await Common.http_request('put', Data.update_url, JSON.stringify(op_data))
    } else {
        results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))
    }

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Delete an actuator
 * 
 * @param {number} actuator_id id of the actuador
 * 
 * @returns {boolean}
 * @throws {object}
 */
export async function delete_actuator (actuator_id) {
    let op_data = {
        resource: Data.Resources.Actuator.name,
        where   : [
            { [Data.Resources.Actuator.fields.id]: actuator_id }
        ]
    }

    let results = 
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))
    
    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


export async function save_action (action, update=false) {
    let op_data = {
        resource: Data.Resources.Action.name,
        data    : action
    }

    let results

    if (update) {
        op_data.where = [
            { [Data.Resources.Action.fields.num]        : action.num },
            { [Data.Resources.Action.fields.actuator_id]: action.actuator_id }
        ]

        op_data.data = {
            [Data.Resources.Action.fields.name]       : action.name, 
            [Data.Resources.Action.fields.description]: action.description,
            [Data.Resources.Action.fields.signal]     : action.signal
        }

        results = await Common.http_request('put', Data.update_url, JSON.stringify(op_data))
    } else {
        results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))
    }

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Delete an action from an actuator
 * 
 * @param {number} action_num  id of the action
 * @param {number} actuator_id id of the actuator
 * 
 * @returns {boolean}
 * @throws {object}
 */
export async function delete_action (action_num, actuator_id) {
    let op_data = {
        resource: Data.Resources.Action.name,
        where   : [
            { [Data.Resources.Action.fields.num]        : action_num },
            { [Data.Resources.Action.fields.actuator_id]: actuator_id }
        ]
    }

    let results = 
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))
    
    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}