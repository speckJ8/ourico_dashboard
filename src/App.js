import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MainPage  from './ui/main_page/MainPage'
import LoginPage from './ui/login_page/LoginPage'

import * as Common from './api/common'


class App extends Component {

    constructor (props) {
        super(props)

        this.state = {
            logged_in: Common.get_token() != null
        }
    }

    render () {
        if (this.state.logged_in)
            return (
                <MainPage 
                    logo={logo} 
                    handle_logged_out={() => this.setState({ logged_in: false })}
                />
            )
        else
            return (
                <LoginPage 
                    logo={logo} 
                    handle_login_done={() => this.setState({ logged_in: true })}
                />
            )
    }
}

export default App;
