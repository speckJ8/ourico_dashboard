
const Resources = {

    Mote: {
        name: 'mote',
        fields: {
            id                     : 'id',
            name                   : 'name',
            location_lat           : 'location_lat',
            location_lng           : 'location_lng',
            active                 : 'active',
            current_ip             : 'current_ip',
            last_status_report     : 'last_status_report',
            max_time_with_no_report: 'max_time_with_no_report',
            crypto_alg             : 'crypto_alg',
            crypto_key             : 'crypto_key',
            purpose_description    : 'purpose_description',
            other_data             : 'other_data',
            all                    : [
                'id', 'name',
                'location_lat', 'location_lng',
                'active', 'current_ip',
                'last_status_report', 'max_time_with_no_report',
                'crypto_alg', 'purpose_description', 'other_data',
                'crypto_key'
            ]
        },
        related: {
            errors         : 'errors',
            configurations : 'configurations',
            status_reports : 'status_reports',
            conns_from_here: 'conns_from_here',
            conns_to_here  : 'conns_to_here',
            sensors        : 'sensors',
            actuators      : 'actuators',
            alerts         : 'alerts',
            triggers       : 'triggers',
            clients        : 'clients',
            api_clients    : 'api_clients',
            packets        : 'packets'
        }
    },


    MoteConfiguration: {
        name: 'mote_configuration',
        fields: {
            mote_id             : 'mote_id',
            name                : 'name',
            measurement_period  : 'measurement_period',
            send_data_period    : 'send_data_period',
            status_report_period: 'status_report_period',
            use_crypto          : 'use_crypto',
            active              : 'active',
            all                 : [
                'name', 'measurement_period', 'mote_id',
                'send_data_period', 'status_report_period',
                'use_crypto', 'active'
            ]
        },
        related: { mote: 'mote' }
    },


    Packet: {
        name: 'packet',
        fields: {
            date_time  : 'date_time',
            packet_type: 'packet_type',
            length     : 'length',
            packet_nr  : 'packet_nr',
            all        : ['date_time', 'packet_type', 'length', 'packet_nr']
        },
        related: { mote: 'mote' }
    },


    MoteError: {
        name: 'mote_error',
        fields: {
            date_time  : 'date_time',
            err_code   : 'err_code',
            err_message: 'err_message',
            all        : ['date_time', 'err_code', 'err_message']
        },
        related: { mote: 'mote' }
    },


    MoteStatusReport: {
        name: 'mote_status_report',
        fields: {
            start_date_time: 'start_date_time',
            end_date_time  : 'end_date_time',
            nr_tx_packets  : 'nr_tx_packets',
            nr_lost_packets: 'nr_lost_packets',
            nr_tx_bytes    : 'nr_tx_bytes',
            nr_lost_bytes  : 'nr_lost_bytes',
            all            : [
                'start_date_time', 'end_date_time',
                'nr_tx_packets', 'nr_lost_packets',
                'nr_tx_bytes', 'nr_lost_bytes'
            ]
        },
        related: { mote: 'mote' }
    },


    MoteConnection: {
        name: 'mote_connection',
        fields: { 
            direction : 'direction',
            mote_id   : 'mote_id',
            other_mote: 'other_mote',
            all: ['direction'] 
        },
        related: {
            mote_here: 'mote_here',
            mote_there: 'mote_there'
        }
    },


    Sensor: {
        name: 'sensor',
        fields: {
            id          : 'id',
            name        : 'name',
            output_size : 'output_size',
            description : 'description',
            async_      : 'async',
            all: [
                'id', 'output_size',
                'description', 'async', 'name'
            ]
        },
        related: {
            parameters: 'parameters',
            uses      : 'uses'
        }
    },


    Parameter: {
        name: 'parameter',
        fields: {
            id         : 'id',
            name       : 'name',
            units      : 'units',
            description: 'description',
            type_      : 'type_',
            size       : 'size',
            position   : 'position',
            all        : [
                'id', 'name', 'units', 'size',
                'description', 'type_', 'position'
            ]
        },
        related: {
            sensor      : 'sensor',
            measurements: 'measurements'
        }
    },


    SensorParam: {
        name: 'sensor_param',
        fields: {
            sensor_id   : 'sensor_id',
            parameter_id: 'parameter_id',
            all         : [ 'sensor_id', 'parameter_id' ]
        },
        related: {
            sensor   : 'sensor',
            parameter: 'parameter'
        }
    },


    SensorUse: {
        name: 'sensor_use',
        fields: {
            num            : 'num',
            interface_num  : 'interface_num',
            interface_name : 'interface_name',
            use_description: 'use_description',
            active         : 'active',
            activated_at   : 'activated_at',
            deactivated_at : 'deactivated_at',
            mote_id        : 'mote_id',
            sensor_id      : 'sensor_id',
            all: [
                'num', 'interface_num', 'mote_id', 'sensor_id',
                'interface_name', 'use_description',
                'active', 'activated_at', 'deactivated_at'
            ]
        },
        related: {
            measurements: 'measurements',
            sensor: 'sensor',
            mote: 'mote'
        }
    },


    Measurement: {
        name: 'measurement',
        fields: {
            date_time: 'date_time',
            value    : 'value',
            all      : [ 'date_time', 'value' ]
        },
        related: {
            sensor_use: 'sensor_use',
            parameter : 'parameter'
        }
    },


    Actuator: {
        name: 'actuator',
        fields: {
            name       : 'name',
            id         : 'id',
            description: 'description',
            all        : [ 'name', 'id', 'description' ]
        },
        related: {
            actions: 'actions',
            uses   : 'uses'
        }
    },


    Action: {
        name: 'action',
        fields: {
            num        : 'num',
            name       : 'name',
            signal     : 'signal',
            description: 'description',
            actuator_id: 'actuator_id',    
            all        : [
                'num', 'name', 'signal', 'description', 'actuator_id'
            ]
        },
        related: {
            results : 'results',
            actuator: 'actuator'
        }
    },


    ActuatorUse: {
        name: 'actuator_use',
        fields: {
            num            : 'num',
            interface_num  : 'interface_num',
            interface_name : 'interface_name',
            use_description: 'use_description',
            active         : 'active',
            activated_at   : 'activated_at',
            deactivated_at : 'deactivated_at',
            mote_id        : 'mote_id',
            actuator_id    : 'actuator_id',
            all            : [
                'num', 'interface_num', 'mote_id', 'actuator_id',
                'interface_name', 'use_description',
                'active', 'activated_at', 'deactivated_at'
            ]
        },
        related: {
            results : 'results',
            actuator: 'actuator',
            mote    : 'mote'
        }
    },


    ActionResult: {
        name: 'action_result',
        fields: {
            date_time: 'date_time',
            result   : 'result',
            all      : [ 'date_time', 'result' ]
        },
        related: {
            action: 'action', actuator_use: 'actuator_use'
        }
    },


    Trigger: {
        name: 'trigger',
        fields: {
            id         : 'id',
            name       : 'name',
            description: 'description',
            all        : ['id', 'name', 'description']
        },
        related: { uses: 'uses' }
    },


    TriggerUse: {
        name: 'trigger_use',
        fields: {
            active        : 'active',
            activated_at  : 'activated_at',
            deactivated_at: 'deactivated_at',
            mote_id       : 'mote_id',
            trigger_id    : 'trigger_id',
            all: [
                'active', 'activated_at', 'deactivated_at', 'mote_id', 'trigger_id'
            ]
        },
        related: {
            occurrences: 'occurrences',
            trigger    : 'trigger',
            mote       : 'mote'
        }
    },


    TriggerOccurrence: {
        name: 'trigger_occurrence',
        fields: {
            date_time: 'date_time',
            all      : [ 'date_time' ]
        },
        related: {
            trigger_use: 'trigger_use'
        }
    },


    Alert: {
        name: 'alert',
        fields: {
            id         : 'id',
            name       : 'name',
            description: 'description',
            level      : 'level',
            all        : [
                'id', 'name', 'description', 'level'
            ]
        },
        related: { 
            uses : 'uses'
        }
    },


    AlertUse: {
        name: 'alert_use',
        fields: {
            mote_id       : 'mote_id',
            alert_id      : 'alert_id',
            active        : 'active',
            activated_at  : 'activated_at',
            deactivated_at: 'deactivated_at',
            all           : [
                'active', 'activated_at', 'deactivated_at', 'mote_id', 'alert_id'
            ]
        },
        related: {
            occurrences: 'occurrences',
            alert      : 'alert',
            mote       : 'mote'
        }
    },


    AlertOccurrence: {
        name: 'alert_occurrence',
        fields: {
            date_time: 'date_time',
            all      : ['date_time']
        },
        related: {
            alert_use: 'alert_use'
        }
    },


    Client: {
        name: 'client',
        fields: {
            id                  : 'id',
            client_id           : 'client_id', // owner
            name                : 'name',
            admin               : 'admin',
            owner               : 'owner',
            username            : 'username',
            password            : 'password',
            email               : 'email',
            write_capability    : 'write_capability',
            compute_capability  : 'compute_capability',
            rt_events_capability: 'rt_events_capability',
            all                 : [
                'id', 'name', 'client_id', 'admin',
                'username', 'password', 'email',
                'write_capability', 'compute_capability', 'rt_events_capability'
            ]
        },
        related: {
            motes          : 'motes',
            api_clients    : 'api_clients',
            logs           : 'logs',
            extension_packs: 'extension_packs',
            owned_users    : 'owned_users',
            owner          : 'owner'
        }
    },


    ApiClient: {
        name: 'api_client',
        fields: {
            id                    : 'id',
            name                  : 'name',
            access_token          : 'access_token',
            write_capability      : 'write_capability',
            compute_capability    : 'compute_capability',
            rt_events_capability  : 'rt_events_capability',
            allowed_origin_address: 'allowed_origin_address',
            client_id             : 'client_id',
            all                 : [
                'id', 'access_token', 'allowed_origin_address', 'name', 'client_id',
                'write_capability', 'compute_capability', 'rt_events_capability'
            ]
        },
        related: {
            owner: 'owner',
            logs : 'logs'
        }
    },

    
    UserLogs: {
        name : 'user_operations_log',
        fields: {
            client_id: 'client_id',
            query    : 'query',
            level    : 'level',
            operation: 'operation',
            message  : 'message',
            date_time: 'date_time',
            all      : [
                'client_id', 'query', 'level',
                'operation', 'message', 'date_time'
            ]
        },
        related: {
            owner: 'owner'
        }
    },


    AppLogs: {
        name : 'api_operations_log',
        fields: {
            api_client_id: 'api_client_id',
            query        : 'query',
            level        : 'level',
            operation    : 'operation',
            message      : 'message',
            date_time    : 'date_time',
            all          : [
                'api_client_id', 'query', 'level',
                'operation', 'message', 'date_time'
            ]
        },
        related: {
            owner: 'owner'
        }
    },


    ExtensionPack: {
        name: 'extension_pack',
        fields: {
            prog_language: 'prog_language',
            code_repo_url: 'code_repo_url',
            active       : 'active'
        },
        related: {
            owner: 'owner',
            logs : 'logs'
        }
    },


    ExtensionPackLogs: {
        name: 'extension_pack_log',
        fields: {
            client_id: 'extension_pack_id',
            level: 'level',
            message: 'message',
            date_time: 'date_time',
            all: [ 'extension_pack_id', 'level', 'message', 'date_time' ]
        },
        related: {
            owner: 'owner'
        }
    }

} 


const base_url   = '/api.data/'
const read_url   = base_url + 'get/'
const write_url  = base_url + 'new/'
const update_url = base_url
const delete_url = base_url
export { Resources, base_url, read_url, write_url, update_url, delete_url }
