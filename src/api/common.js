import * as Control from './control'


// key used to save and retrieve authentication token from browser's storage
const STORAGE_TK_KEY = 'ourico.auth_token'


/**
 * Save the authentication token to the browser storage.
 * It will be saved `localStorage` so that it can persist among sessions.
 * @param {string} token
 */
export function set_token (token) {
    window.localStorage.setItem(STORAGE_TK_KEY, token)
}


/**
 * Get token from browser storage
 * @return the token
 */
export function get_token () {
    return window.localStorage.getItem(STORAGE_TK_KEY)
}


/**
 * Remove token from browser storage
 */
export function remove_token () {
    window.localStorage.removeItem(STORAGE_TK_KEY)
}


/**
 * Represents a response to an HTTP request made to the server.
 * If `status` is 200 `data` will be set, otherwise  `error_msg` will be set.
 * `status` will be -1 if the request failed
 */
export class ApiResponse {

    constructor (status, data, error_msg) {
        this.status    = status
        this.data      = data
        this.error_msg = error_msg
    }

    is_error () { return this.status !== 200 }
}



/**
 * Makes a POST request handling the authentication steps
 *
 * @param {string} method
 * @param {string} url
 * @param {string} data      data to send in post request (not required if `method` is get)
 * @param {string} body_type mime type of `data` [default = json]
 *
 * @return ApiResponse with json object in data field if request successful
 */
export async function
http_request (method, url, data = undefined, body_type = 'application/json') {
    try {

        let request_data = {
            method: method,
            headers: { 'Authorization': `Bearer ${get_token()}` },
        }
        if (method !== 'get' && method !== 'GET') {
            request_data.body = data
            request_data.headers['Content-Type'] = body_type
        }

        let response = await window.fetch(url, request_data)
        let the_response
        if (response.headers.has('WWW-Authenticate')) {
            // most likely token update necessary
            let response = await Control.update_token(get_token())
            if (response.status === -1) {
                return response
            } else if (response.status !== 200) {
                return new ApiResponse(-1, null, `Não foi possivel atualizar as \
                    credenciais de autenticação. Novo inicio de sessão poderá ser necessário`)
            }
            set_token(response.data)
            let new_token = response.data
            console.log('new_token = ',new_token)
            request_data.headers['Authorization'] = `Bearer ${new_token}`
            // try again
            the_response = await window.fetch(url, request_data)
        } else {
            the_response = response
        }

        if (the_response.status === 404) {
            return new ApiResponse(the_response.status, {}, '404 Resource not found')
        } else if (the_response.status >= 500) {
            return new ApiResponse(the_response.status, {}, 'Erro no servidor')
        } else if (the_response.status === 401 || the_response.status === 403) {
            return new ApiResponse(the_response.status, {}, 'A operação não foi autorizada')
        } else if (the_response.status !== 200) {
            let result = await the_response.text()
            return new ApiResponse(the_response.status, {}, result)
        }

        return new ApiResponse(200, await the_response.json(), null)

    } catch (exception) {
        console.error('XXXXXXXX', exception)
        return new ApiResponse(-1, null, 'Pedido ao servidor falhou')
    }
}
